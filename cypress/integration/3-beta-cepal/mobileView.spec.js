describe('check views in mobile version', () => {
  beforeEach(() => {
    cy.visit('http://webpro.cepal.org/es')
    cy.viewport('iphone-6')
  })

  it('scrolling' , () => {
    cy.get('.flex-active-slide')
      .scrollIntoView({ duration: 3000 })
    cy.get('.pane-node-field-section-news-translated')
      .scrollIntoView({ duration: 3000 })
    cy.get('.pane-node-field-section-publications > .pane-title')
      .scrollIntoView({ duration: 3000 })
    cy.get('.pane-node-field-section-video-translated > .pane-title')
      .scrollIntoView({ duration: 3000 })
    cy.scrollTo('bottom', { duration: 3500 })

  })
  
})
