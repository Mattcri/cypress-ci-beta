describe('Get in the CEPAL test environment', () => {
  beforeEach(() => {
    cy.visit('http://webpro.cepal.org/es')
  })

  it('spanish language', () => {
    cy.get('.language-link').should('include.text', 'Español')
  })

  it('open mobile menu', () => {
    cy.get('.block__title').click()
    cy.get('.block__title').click()
    
  })

  it('scroll to bottom and click up', () => {
    cy.scrollTo('bottom')
    cy.get('#backtotop').click()
  })

  it('change to english language', () => {
    cy.get(':nth-child(2) > .language-link')
      .should('include.text', 'English' )
      .click()
    cy.url().should('eq', 'http://webpro.cepal.org/en')
  })


})