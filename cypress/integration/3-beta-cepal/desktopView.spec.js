describe('Get in the CEPAL test environment', () => {
  beforeEach(() => {
    cy.visit('http://webpro.cepal.org/es')
    cy.viewport('macbook-13')
  })

  it('search and type in search navigation', () => {
    cy.get('#edit-search-term')
      .type('chile{enter}')
      .wait(2000)
    cy.get('[aria-label="Página 2"]')
      .click()
      .wait(2000)
    
  })

  it('check aside color and min-height', () => {
    cy.get('.home-services > ul > :nth-child(1)')
      .should('have.css', 'background-color', 'rgb(230, 230, 230)')
      .should('have.css', 'min-height', '58.5px')
      
  })

})